import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import searchLeases from '@salesforce/apex/Peskov_Info_Lease_Search.search';

export default class Peskov_info_prev_lease extends LightningElement {

    @api notifyViaAlerts = false;
    @api prevLeaseName;
    @api prevLeaseId;

    @track isMultiEntry = false;
    @track initialSelection = [];
    @track errors = [];

    connectedCallback() {
        if (this.prevLeaseName) {
            this.initialSelection[0] = {
                title: this.prevLeaseName, 
                id: this.prevLeaseId,
                icon: 'standard:opportunity',
            };
        }
    }

    handleSearch(event) {
        searchLeases(event.detail)
            .then(results => {
                this.template
                    .querySelector('c-lookup')
                    .setSearchResults(results);
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.',
                    'error'
                );
                // eslint-disable-next-line no-console
                console.error('Lookup error', JSON.stringify(error));
                this.errors = [error];
            });
    }

    handleSelectionChange() {
        const selection = this.template
            .querySelector('c-lookup')
            .getSelection();

        if (selection.length > 0) {
            this.prevLeaseName = selection[0].title;
            this.prevLeaseId = selection[0].id;
        } else {
            this.prevLeaseName = undefined;
            this.prevLeaseId = undefined;
        }
        
        const prevLease = new CustomEvent('lease_prevlease_changed', {
            detail: {
                prevLeaseName: this.prevLeaseName,
                prevLeaseId: this.prevLeaseId
            },
            bubbles: true,
            composed: true,
        });
        this.dispatchEvent(prevLease);

        this.errors = [];
    }

    checkForErrors() {
        const selection = this.template
            .querySelector('c-lookup')
            .getSelection();
        if (selection.length === 0) {
            this.errors = [
                { message: 'You must make a selection before submitting!' },
                { message: 'Please make a selection and try again.' }
            ];
        } else {
            this.errors = [];
        }
    }

    notifyUser(title, message, variant) {
        if (this.notifyViaAlerts) {
            // Notify via alert
            // eslint-disable-next-line no-alert
            alert(`${title}\n${message}`);
        } else {
            // Notify via toast
            const toastEvent = new ShowToastEvent({ title, message, variant });
            this.dispatchEvent(toastEvent);
        }
    }
}