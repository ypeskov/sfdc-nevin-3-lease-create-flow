/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import getAllWarehouses from '@salesforce/apex/Peskov_Info_Lease_Warehouse.getAllWarehouses';
import getWarehousePrice from '@salesforce/apex/Peskov_Info_Lease_Warehouse.getWarehousePrice';

export default class lease_warehouse extends LightningElement {
    @api currentWarehouse;
    @track warehouses = [];

    @track warehouse;

    connectedCallback() {
        this.warehouse = {...this.currentWarehouse};

        getAllWarehouses()
        .then(result => {
            this.warehouses = result.map(item => {
                return { label: item.Name, value: item.Id, size: parseInt(item.Warehouse_Size__c, 10) };
            });
            
            if (this.warehouse.value.length === 0) {
                this.warehouse = this.warehouses[0];
                this.handleChange({detail: {value: this.warehouse.value}});
            }
        })
        .catch(error => {console.log(error);});
    }

    handleChange(event) {
        this.warehouse = this.warehouses.reduce((total=null, item) => {
            if (event.detail.value === item.value) {
                total = item;
            }
            return total;
        });
        
        getWarehousePrice({Id: this.warehouse.value})
            .then(result => {
                const rate = parseInt(result, 10);
                this.warehouse.rate = rate;
                this.warehouse.totalPrice = rate * this.warehouse.size;

                this.fireWareHouseChanged();
            })
            .catch(error =>console.log(error));

        this.fireWareHouseChanged();
    }

    fireWareHouseChanged() {
        const whChanged = new CustomEvent('warehouse_changed', {
            detail: {
                warehouse: this.warehouse
            },
            bubbles: true,
            composed: true,
        });
        this.dispatchEvent(whChanged);
    }
}