/* eslint-disable no-console */
import { LightningElement, track, api } from 'lwc';
import getPaymentMethodValues from '@salesforce/apex/Peskov_Info_Datasources.getPaymentMethodValues';

export default class lease_payment_method extends LightningElement {
    @track value;
    @track options;

    @api paymentMethod;
    @api paymentId;

    constructor() {
        super();

        getPaymentMethodValues()
            .then(result => {
                this.options = result.map(method => {
                    return {label: method, value: method};
                });
            })
            .catch(error => console.log(error));
    }

    connectedCallback() {
        this.value = this.paymentMethod;
    }

    handleChange(event) {
        this.value = event.detail.value;

        const paymentMethodChanged = new CustomEvent('payment_method_changed', {
            detail: {
                paymentId: this.paymentId,
                paymentMethod: this.value,
            }, 
            bubbles: true,
        });

        this.dispatchEvent(paymentMethodChanged);
    }
}