/* eslint-disable no-console */
import { LightningElement, track, api } from 'lwc';
import getLeaseTypes from '@salesforce/apex/Peskov_Info_Lease.getLeaseTypes';

export default class Peskov_info_lease_type extends LightningElement {
    @track options;
    @track value;

    @api leaseType;

    constructor() {
        super();

        getLeaseTypes()
        .then(result => {
            this.options = result.map(type => {return {label: type, value: type}; });
        })
        .catch(error => console.log(error));
    }

    connectedCallback() {
        this.value = this.leaseType;
    }

    handleChange(event) {
        this.value = event.detail.value;

        const leaseTypeChanged = new CustomEvent('lease_type_changed', {
            detail: {
                leaseType: this.value,
            }, 
            bubbles: true,
            composed: true
        });

        this.dispatchEvent(leaseTypeChanged);
    }
}