import { LightningElement, api } from 'lwc';

export default class lease_modal_header extends LightningElement {
    @api headerLabel;
    @api leaseName;

    get fullHeaderLabel() {
        const leaseName = this.leaseName || '';
        
        return `${this.headerLabel} [${leaseName}]`;
    }
}