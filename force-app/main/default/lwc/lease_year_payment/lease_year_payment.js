import { LightningElement, track, api } from 'lwc';

export default class lease_year_payment extends LightningElement {
    @track value;
    @api year;

    maxNumberOfPayments = 10;

    get options() {
        let options = [];
        for(let i=0; i<this.maxNumberOfPayments; i++) {
            options[i] = {
                label: i+1,
                value: i+1,
            }
        }

        return options;
    }

    handleChange(event) {
        this.value = event.detail.value;

        const changedEvent = new CustomEvent('paymentchanged', {
            detail: parseInt(this.value, 10),
            bubbles: true
        });

        this.dispatchEvent(changedEvent);
    }
}