/* eslint-disable no-console */
import { LightningElement,api } from 'lwc';

export default class lease_payment_amount extends LightningElement {
    @api amount;
    @api paymentId;
    @api isDisabled;

    updateAmount(e) {
        this.amount = parseInt(e.detail.value, 10)
        const amountChangedEvt = new CustomEvent('amount_changed', {
            detail: {
                id: this.paymentId,
                amount: parseInt(e.detail.value, 10),
            },
            bubbles: true,
            composed: true,
        });

        this.dispatchEvent(amountChangedEvt);
    }
}