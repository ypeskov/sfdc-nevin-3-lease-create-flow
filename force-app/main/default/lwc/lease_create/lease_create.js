/* eslint-disable no-undef */
/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';

export default class lease_create extends LightningElement {
    @api startDate;
    @api closeDate;
    @api accountName;
    @api accountId;
    @api endDate;
    @api numberOfYears;
    @api leaseName;
    @api warehouse;
    @api leaseType;
    @api prevLeaseName;
    @api prevLeaseId;
    @api gracePeriod;

    @track gracePeriodValue;

    renderedCallback() {
        const validationEvent = new CustomEvent('lease_create_validation', {
            detail: {
                isValid: () => {
                    let isValid = true;

                    const leaseNameInput = this.template.querySelectorAll('lightning-input')[0];
                    if (!leaseNameInput.checkValidity()) {
                        leaseNameInput.reportValidity();
                        isValid = false;
                    }

                    const closeDateInput = this.template.querySelectorAll('lightning-input')[3];
                    if (!closeDateInput.checkValidity()) {
                        closeDateInput.reportValidity();
                        isValid = false;
                    }

                    return isValid;
                }
            },
            bubbles: true,
        });
        this.dispatchEvent(validationEvent);
    }

    connectedCallback() {
        this.gracePeriodValue = this.gracePeriod;

        this.calculateEndDate();
        this.fireChangeEvent();
    }

    handleGracePeriodChange(e) {
        this.gracePeriodValue = e.target.value;

        const changedEvent = new CustomEvent('graceperiod_changed', {
            detail: parseInt(this.gracePeriodValue, 10),
            bubbles: true
        });

        this.dispatchEvent(changedEvent);
    }

    handleStarDateChange(e) {
        this.startDate = moment(e.target.value);

        this.calculateEndDate();
        this.fireChangeEvent();
    }

    handleCloseDateChange(e) {
        this.closeDate = moment(e.target.value);
        this.fireChangeEvent();
    }

    handleLeasenameChange(e) {
        this.leaseName = e.target.value;

        this.fireChangeEvent();
    }

    get startDateFormatted() {
        return moment(this.startDate).format();
    }

    get closeDateFormatted() {
        return moment(this.closeDate).format();
    }

    get endDateFormatted() {
        return moment(this.endDate).add(this.gracePeriod, 'd').format();
    }

    get showPrevLease() {
        return this.leaseType === 'Renewed Lease';
    }

    handleYearsChange(e) {
        this.numberOfYears = parseInt(e.target.value, 10);

        this.dispatchEvent(new CustomEvent('years_changed', {
            detail: {
                numberOfYears: this.numberOfYears
            },
            bubbles: true,
        }));

        this.calculateEndDate();
        this.fireChangeEvent();
    }

    calculateEndDate() {
        this.endDate = this.startDate.clone().add(this.numberOfYears, 'y').subtract(1, 'd');
    }

    fireChangeEvent() {
        const changedEvent = new CustomEvent('lease_create_changed', {
            detail: {
                startDate: this.startDate,
                closeDate: this.closeDate,
                endDate: this.endDate,
                leaseName: this.leaseName,
                numberOfYears: this.numberOfYears,
            },
            bubbles: true
        });

        this.dispatchEvent(changedEvent);
    }
}