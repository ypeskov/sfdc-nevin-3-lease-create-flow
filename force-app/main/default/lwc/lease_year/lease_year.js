/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';

export default class lease_year extends LightningElement {
    @api year;
    @api payments;
    @api stardDate;
    @api endDate;
    
    @track innerYear;

    constructor() {
        super();

        this.template.addEventListener('paymentchanged', this.handleNumberOfPaymentsChanged.bind(this));
        this.template.addEventListener('amount_changed', this.handleAmountChange.bind(this));
        this.template.addEventListener('payment_method_changed', this.handlePaymentMethodChanged.bind(this));
        this.template.addEventListener('payment_type_changed', this.handlePaymentTypeChanged.bind(this));
    }

    connectedCallback() {
        this.innerYear = {...this.year};
        this.innerYear.payments = this.innerYear.payments.map(payment => { return JSON.parse(JSON.stringify(payment)); });
    }

    handlePaymentMethodChanged(e) {
        const paymentId = parseInt(e.detail.paymentId, 10);
        const method = e.detail.paymentMethod;

        this.innerYear.payments.forEach(payment => {
            if (payment.id === paymentId) {
                payment.method = method;
            }
        });

        this.fireYearUpdate();
    }

    handlePaymentTypeChanged(e) {
        const paymentId = parseInt(e.detail.paymentId, 10);
        const type = e.detail.paymentType;

        this.innerYear.payments.forEach(payment => {
            if (payment.id === paymentId) {
                payment.type = type;
            }
        });

        this.fireYearUpdate();
    }

    handleRateChange(e) {
        const rate = parseInt(e.detail.value, 10);
        this.innerYear.rate = rate;
        this.innerYear.totalPrice = rate * this.innerYear.size;

        const averagePayment = this.innerYear.totalPrice / this.innerYear.payments.length;
        this.innerYear.payments.forEach(payment => {
            payment.amountWithoutVAT = averagePayment;
        });

        this.fireYearUpdate();
    }

    handleNumberOfPaymentsChanged(e) {
        this.innerYear.payments = [];
        const numberOfPayments = e.detail;

        for(let i=0; i < numberOfPayments; i++) {
            this.innerYear.numberOfPayments = numberOfPayments;
            this.innerYear.payments[i] = {
                id: i,
                type: '',
                method: '',
                amountWithoutVAT: 0,
            };

            if (i === numberOfPayments-1) {
                this.innerYear.payments[i].isDisabled = 'disabled'
            } else {
                this.innerYear.payments[i].isDisabled = undefined
            }
        }
        this.handleRateChange({detail: {value:this.innerYear.rate}});

        this.fireYearUpdate();
    }

    handleAmountChange(e) {
        const paymentId = parseInt(e.detail.id, 10);
        let amount = parseInt(e.detail.amount, 10);
        const paymentsLength = this.innerYear.payments.length

        if (paymentId !== paymentsLength-1) { 
            this.innerYear.payments[paymentId].amountWithoutVAT = amount;

            let acc = 0
            for(let i=0; i < paymentsLength-1; i++) {
                acc += this.innerYear.payments[i].amountWithoutVAT
            }
    
            this.innerYear.payments[paymentsLength-1].amountWithoutVAT = this.year.totalPrice - acc
        }

        this.fireYearUpdate();
    }

    fireYearUpdate() {
        const yearUpdatedEvt = new CustomEvent('year_changed', {
            detail: {
                year: {...this.innerYear},
            },
            bubbles: true,
            composed: true,
        });

        this.dispatchEvent(yearUpdatedEvt);
    }
}