/* eslint-disable no-console */
import { LightningElement, track, api } from 'lwc';
// import getPaymentTypeValues from '@salesforce/apex/Peskov_Info_Datasources.getPaymentTypeValues';

export default class lease_payment_type extends LightningElement {
    @track value;
    @track options = [];

    @api paymentType;
    @api paymentId;

    defaultValue = {label: 'Rental', value: 'Rental'};

    constructor() {
        super();

        //  the new requirement is to have only "Rental" option

        // getPaymentTypeValues()
        // .then(result => {
        //     this.options = result.map(type => {return {label: type, value: type}; });
        // })
        // .catch(error => console.log(error));

        this.options = [this.defaultValue];
    }

    connectedCallback() {
        // this.value = this.paymentType;
        //  the new requirement is to have only "Rental" option
        this.value = 'Rental';
        this.fireDataUpdateEvent();
    }

    handleChange(event) {
        this.value = event.detail.value;
        this.fireDataUpdateEvent();
    }

    fireDataUpdateEvent() {
        const paymentTypeChanged = new CustomEvent('payment_type_changed', {
            detail: {
                paymentId: this.paymentId,
                paymentType: this.value,
            }, 
            bubbles: true,
        });

        this.dispatchEvent(paymentTypeChanged);
    }
}