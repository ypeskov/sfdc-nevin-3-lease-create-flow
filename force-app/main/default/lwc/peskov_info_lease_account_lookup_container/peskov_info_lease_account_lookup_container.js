/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import apexSearch from '@salesforce/apex/SampleLookupController.search';

export default class Peskov_info_lease_account_lookup_container extends LightningElement {

    @api notifyViaAlerts = false;
    @api accountName;
    @api accountId;

    @track isMultiEntry = false;
    @track initialSelection = [];
    @track errors = [];

    connectedCallback() {
        if (this.accountName) {
            this.initialSelection[0] = {
                title: this.accountName, 
                id: this.accountId,
                icon: 'standard:account',
            };
        }
    }

    handleSearch(event) {
        apexSearch(event.detail)
            .then(results => {
                this.template
                    .querySelector('c-lookup')
                    .setSearchResults(results);
            })
            .catch(error => {
                this.notifyUser(
                    'Lookup Error',
                    'An error occured while searching with the lookup field.',
                    'error'
                );
                // eslint-disable-next-line no-console
                console.error('Lookup error', JSON.stringify(error));
                this.errors = [error];
            });
    }

    handleSelectionChange() {
        const selection = this.template
            .querySelector('c-lookup')
            .getSelection();

        if (selection.length > 0) {
            this.accountName = selection[0].title;
            this.accountId = selection[0].id;
        } else {
            this.accountName = undefined;
            this.accountId = undefined;
        }
        
        const accountChanged = new CustomEvent('lease_account_changed', {
            detail: {
                accountName: this.accountName,
                accountId: this.accountId
            },
            bubbles: true,
            composed: true,
        });
        this.dispatchEvent(accountChanged);
        
        this.errors = [];
    }

    checkForErrors() {
        const selection = this.template
            .querySelector('c-lookup')
            .getSelection();
        if (selection.length === 0) {
            this.errors = [
                { message: 'You must make a selection before submitting!' },
                { message: 'Please make a selection and try again.' }
            ];
        } else {
            this.errors = [];
        }
    }

    notifyUser(title, message, variant) {
        if (this.notifyViaAlerts) {
            // Notify via alert
            // eslint-disable-next-line no-alert
            alert(`${title}\n${message}`);
        } else {
            // Notify via toast
            const toastEvent = new ShowToastEvent({ title, message, variant });
            this.dispatchEvent(toastEvent);
        }
    }
}