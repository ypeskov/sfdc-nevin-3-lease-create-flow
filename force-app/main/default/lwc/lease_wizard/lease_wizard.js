/* eslint-disable no-undef */
/* eslint-disable no-console */
import { LightningElement, track, api, wire } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import momentJS from '@salesforce/resourceUrl/moment';
import createLease from '@salesforce/apex/Peskov_Info_Lease.createLease';
import getAccountName from '@salesforce/apex/Peskov_Info_Datasources.getAccountNameById';

import { getRecord } from 'lightning/uiRecordApi';
const FIELDS = ['Opportunity.Id', 'Opportunity.Name',];

export default class Lease_wizard extends NavigationMixin(LightningElement) {
    CREATE_LEASE = 'Create a Lease';
    EDIT_YEARS = 'Edit Years';

    @api recordId;
    @api objectApiName;

    currentStep;

    @track warehouse;
    @track stepCreateLease = true;
    @track stepEditYears = false;
    @track headerLabel;
    @track prevLabel;
    @track nextLabel;
    @track isModalOpened = false;
    @track startDate;
    @track closeDate;
    @track endDate;
    @track numberOfYears = 1;
    @track leaseName = '';
    @track years = [];
    @track leaseType = 'New Lease';
    @track prevLease;
    @track accountName;
    @track accountId;
    @track prevLeaseName;
    @track prevLeaseId;
    @track buttonName;
    @track gracePeriod = 0;

    steps = {
        0: { 
            name: 'cancel', 
            value: this.closeModal, 
            prev: '', 
            next: '' 
        },
        1: { 
            name: 'createLease', 
            value: this.createLease, 
            prev: 'Cancel', 
            next: 'Next' 
        },
        2: { 
            name: 'editYears', 
            value: this.editYears, 
            prev: 'Back', 
            next: 'Create Lease and Close' 
        },
        3: { 
            name: 'finish', 
            value: this.finish, 
            prev: 'Back', 
            next: 'Create Lease and Close' 
        },
    };

    constructor() {
        super();

        if (!this.warehouse) {
            this.warehouse = {
                rate: 0,
                size: 0,
                label: '',
                value: '',
                totalPrice: 0,
            }
        }

        Promise.all([ loadScript(this, momentJS) ])
            .then(() => {
                if (!this.startDate) {
                    this.startDate = moment();
                }

                if (!this.closeDate) {
                    this.closeDate = moment(this.closeDate).format();
                }

                this.updateYears();

            })
            .catch(error => { console.log(error); });

        this.template.addEventListener('lease_create_changed', this.handleLeaseCreateChange.bind(this));
        this.template.addEventListener('warehouse_changed', this.warehouseChanged.bind(this));
        this.template.addEventListener('years_changed', this.handleYearsChange.bind(this));
        this.template.addEventListener('year_changed', this.handleYearChange.bind(this));
        this.template.addEventListener('lease_type_changed', this.handleLeaseTypeChange.bind(this));
        this.template.addEventListener('lease_account_changed', this.handleAccountChange.bind(this));
        this.template.addEventListener('lease_prevlease_changed', this.handlePrevLeaseChange.bind(this));
        this.template.addEventListener('lease_create_validation', this.handleLeaseValidation.bind(this));
        this.template.addEventListener('graceperiod_changed', this.handleGracePeriodChange.bind(this));
    }

    connectedCallback() {
        getAccountName({
            recordId: this.recordId,
            objectApiName: this.objectApiName
        })
        .then(result => {
            this.accountId = result.Id;
            this.accountName = result.Name;
        })
        .catch(error => console.log(error));

        if (this.objectApiName === 'Opportunity') {
            this.leaseType = 'Renewed Lease';
            this.buttonName = 'Renew Lease'
        } else {
            this.leaseType = 'New Lease';
            this.buttonName = 'Add New Lease';
        }
    }

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({error, data}) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
        } else if (data) {
            if (this.objectApiName === 'Opportunity') {
                this.prevLease = data;
                this.prevLeaseId = data.fields.Id.value;
                this.prevLeaseName = data.fields.Name.value;
            }
        }
    } 

    handleGracePeriodChange(e) {
        this.gracePeriod = e.detail;

        this.updateYears();
    }

    handleLeaseValidation(e) {
        this.steps[1].valid = e.detail.isValid;
    }

    handleYearsChange(e) {
        this.numberOfYears = e.detail.numberOfYears;
        this.updateYears();
    }

    handleAccountChange(e) {
        this.accountName = e.detail.accountName;
        this.accountId = e.detail.accountId;
    }

    handlePrevLeaseChange(e) {
        this.prevLeaseName = e.detail.prevLeaseName;
        this.prevLeaseId = e.detail.prevLeaseId;
    }

    handleYearChange(e) {
        for(let i=0; i < this.years.length; i++) {
            if (this.years[i].year === e.detail.year.year) {
                this.years[i] = {...e.detail.year};
            }
        }
    }

    handleLeaseTypeChange(e) {
        this.leaseType = e.detail.leaseType;
    }

    warehouseChanged(e) {
        this.warehouse = e.detail.warehouse;

        this.years.forEach(year => {
            year.rate = this.warehouse.rate;
            year.size = this.warehouse.size;
            year.totalPrice = this.warehouse.rate * this.warehouse.size;
        });
    }

    updateYears() {
        const now = moment();

        this.years = [];
        for(let i=0; i < this.numberOfYears; i++) {
            const rate = this.warehouse.rate;
            const size = this.warehouse.size;
            const total = rate * size;

            
            const yearEndDate = 
                this.startDate.clone().add(i+1, 'y').subtract(1, 'd').add(this.gracePeriod, 'd').format('YYYY-MM-DD');

            let yearStartDate = this.startDate.clone().add(i, 'y');
            //for all the next years we want the start date to take into account the grace period
            if (i > 0) {
                yearStartDate.add(this.gracePeriod, 'd');
            }
            yearStartDate = yearStartDate.format('YYYY-MM-DD');

            this.years[i] = {
                year: now.clone().add(i, 'y').format('Y'),
                order: i,
                startYearDate: yearStartDate,
                endYearDate: yearEndDate,
                rate: rate,
                size: size,
                totalPrice: total,
                numberOfPayments: 0,
                payments: []
            }
        }
    }

    handleLeaseCreateChange(e) {
        this.startDate = e.detail.startDate;
        this.closeDate = e.detail.closeDate;
        this.endDate = e.detail.endDate;
        this.leaseName = e.detail.leaseName;
        this.numberOfYears = e.detail.numberOfYears;

        this.updateYears();
    }

    openModal() {
        this.isModalOpened = true;
        this.currentStep = 1;
        this.headerLabel = this.CREATE_LEASE;
        this.stepCreateLease = true;
        this.stepEditYears = false;

        const stepObj = this.steps[this.currentStep];
        this.prevLabel = stepObj.prev;
        this.nextLabel = stepObj.next;
    }

    closeModal() {
        this.isModalOpened = false;
    }

    prevStep() {
        const nextStep = this.steps[--this.currentStep];
        this.applyStepChange(nextStep);
    }

    nextStep() {
        const currStep = this.steps[this.currentStep];
        const nextStep = this.steps[++this.currentStep];

        if (currStep.valid && currStep.valid() || !currStep.valid) {
            this.applyStepChange(nextStep);
        } else {
            this.currentStep--;
        }
    }

    applyStepChange(nextStep) {
        const action = nextStep.value.bind(this);
        this.prevLabel = nextStep.prev;
        this.nextLabel = nextStep.next;

        action();
    }

    createLease() {
        this.stepCreateLease = true;
        this.stepEditYears = false;
        this.headerLabel = this.CREATE_LEASE;
    }

    editYears() {
        this.stepCreateLease = false;
        this.stepEditYears = true;
        this.headerLabel = this.EDIT_YEARS;
    }

    finish() {
        // console.log(JSON.stringify(this.warehouse));
        // console.log(this.years);

        createLease({
            leaseName: this.leaseName,
            leaseType: this.leaseType,
            prevLeaseId: this.prevLeaseId,
            accountId: this.accountId,
            closeDateStr: this.closeDate,
            startDateStr: this.startDate.format('YYYY-MM-DD'),
            endDateStr: this.endDate.format('YYYY-MM-DD'),
            warehouse: JSON.stringify(this.warehouse),
            years: JSON.stringify(this.years),
            gracePeriod: this.gracePeriod,
        })
        .then(result => {
            this.closeModal();

            this[NavigationMixin.GenerateUrl]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: result.Id,
                    actionName: 'view',
                },
            })
            .then(url => {
                const event = new ShowToastEvent({
                    "title": "Lease",
                    "variant": 'success',
                    mode: 'dismissable',
                    "message": "Record {0} created. See it {1}.",
                    "messageData": [
                        'Lease',
                        {
                            url,
                            label: 'here'
                        }
                    ]
                });

                this.dispatchEvent(event);
            })
        })
        .catch(error => { 
            console.log(error);

            this.dispatchEvent(new ShowToastEvent({
                "title": "Lease creating error",
                "variant": 'error',
                "message": "Error creating the lease. Try again please.",
            }));

            this.currentStep--;
        });
    }

    get startDateFormatted() {
        // eslint-disable-next-line no-undef
        return moment(this.startDate).format();
    }

    get endDateFormatted() {
        // eslint-disable-next-line no-undef
        return moment(this.endDate).add(this.gracePeriod, 'd').format();
    }
}