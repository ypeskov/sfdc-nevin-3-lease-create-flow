public with sharing class Peskov_Info_Datasources {
    @AuraEnabled(cacheable=true)
    public static List<String> getPaymentTypeValues(){
        String[] values = new List<String>();
        
        Schema.DescribeFieldResult fieldResult = Payment__c.Payment_Type__c.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry pickListVal : ple){
            values.add(pickListVal.getLabel());
        }

        return values;
    }

    @AuraEnabled(cacheable=true)
    public static List<String> getPaymentMethodValues(){
        String[] values = new List<String>();
        
        Schema.DescribeFieldResult fieldResult = Payment__c.Payment_Method__c.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry pickListVal : ple){
            values.add(pickListVal.getLabel());
        }

        return values;
    }

    @AuraEnabled(cacheable=true)
    public static Account getAccountNameById(String recordId, String objectApiName) {
        Account acc = new Account();

        if (objectApiName == 'Account') {
            acc = Database.query('SELECT Id, Name FROM Account WHERE Id = \'' + String.escapeSingleQuotes(recordId) + '\'');
        }
        
        if (objectApiName == 'Opportunity') {
            String query = 'SELECT Account.Id, Account.Name FROM Opportunity WHERE Id = \'' 
                            + String.escapeSingleQuotes(recordId) + '\'';
            Opportunity opp = Database.query(query);
            acc.Id = opp.Account.Id;
            acc.Name = opp.Account.Name;
        }

        return acc;
    }
}
