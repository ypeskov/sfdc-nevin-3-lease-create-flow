public with sharing class Peskov_Info_Year {
    public Integer order {get;set;}
    public Decimal rate {get;set;}
    public Decimal totalPrice {get;set;}
    public String startYearDate {get;set;}
    public String endYearDate {get;set;}

    public Peskov_Info_Year_Payment[] payments;
}