@isTest
public class UpdateOnActualOccupiedTest {
    @isTest
    public static void testUpdateOnActualOccupiedtrigger() {
        Date d = Date.valueOf('2025-10-10');
        Targets__c target = new Targets__c(Date__c=d);
        insert target;

        Actual_Occupied__c ao = new Actual_Occupied__c(Created_Date__c=d);
        insert ao;
    }
}
