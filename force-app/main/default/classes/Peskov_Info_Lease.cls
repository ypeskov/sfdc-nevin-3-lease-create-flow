public with sharing class Peskov_Info_Lease {
    public class WhClass {
        public String label {get;set;}
        public String value {get;set;}
    }

    @AuraEnabled
    public static Opportunity createLease(String leaseName, 
                                    String leaseType,
                                    String prevLeaseId,
                                    String accountId,
                                    String closeDateStr,
                                    String startDateStr,
                                    String endDateStr,
                                    String warehouse,
                                    String years,
                                    Integer gracePeriod
                                    ) {

        //The list of years in the lease that is being created
        Peskov_Info_Year[] yearsList = (Peskov_Info_Year[]) JSON.deserialize(years, Peskov_Info_Year[].class);

        Date startDate = Date.valueOf(startDateStr);
        Date closeDate = Date.valueOf(closeDateStr);
        Date endDate = Date.valueOf(endDateStr);

        //a temp object to keep info about warehouse and use later
        WhClass wh = (WhClass) JSON.deserialize(warehouse, WhClass.class);

        Opportunity lease = new Opportunity(
                                            Name=leaseName,
                                            Lease_Type_Status__c=leaseType,
                                            Previous_Lease__c=prevLeaseId,
                                            AccountId=accountId,
                                            CloseDate=closeDate, 
                                            StageName='Draft',
                                            Lease_Period_Years__c=String.valueOf(yearsList.size()),
                                            Lease_End_Date__c=endDate,
                                            Lease_Start_Date__c=startDate,
                                            Warehouse__c=wh.label,
                                            Grace_Period_Days__c=gracePeriod
                                            );
        insert lease;

        //get info about pricebook record to add it later into OpportunityLineItem(Lease item)
        String query = 'SELECT Id, Name, Pricebook2Id, Product2Id, UnitPrice ' + 
                'FROM PricebookEntry where Product2Id = \'' + String.escapeSingleQuotes(wh.value) + '\' ';
        PricebookEntry pbe = (PricebookEntry) Database.query(query)[0];
        
        // Create Warehouse aka OpportunityLineItem
        OpportunityLineItem oppItem = new OpportunityLineItem(OpportunityId=lease.Id,
                                    PricebookEntryId=pbe.Id,
                                    UnitPrice=pbe.UnitPrice,
                                    ServiceDate=startDate,
                                    Active__c=true,
                                    Years__c=String.valueOf(yearsList.size()),
                                    Quantity=1);
        insert oppItem;

        Year__c[] yearsObjList = new List<Year__c>();
        Payment__c[] paymentsObjList = new List<Payment__c>();

        /** Start creation of related years and payments */
        for(Peskov_Info_Year yearSrc : yearsList) {
            Date yearStartDate = Date.valueOf(yearSrc.startYearDate);
            Integer numberOfPayments = yearSrc.payments.size();

            Year__c year = new Year__c(Lease__c=lease.Id,
                            Rate__c=yearSrc.rate,
                            Period__c=String.valueOf(yearSrc.order + 1),
                            Company__c=Lease.AccountId,
                            Start_Date__c=yearStartDate,
                            End_Date__c=Date.valueOf(yearSrc.endYearDate),
                            //Orginaly_Year_Total__c=(Decimal)yearSrc.totalPrice,//
                            Original_Rent_Ex_VAT__c=(Decimal)yearSrc.totalPrice,
                            Warehouse__c=wh.value,
                            No_Of_Installments__c=String.valueOf(yearSrc.payments.size()),
                            CurrencyIsoCode='AED');
            
            //we have to insert a year in a loop, because need it's id later for payments creation
            //in general there will be not many SOQL requests
            insert year;

            Integer paymentOrder = 0;
            for(Peskov_Info_Year_Payment paymentSrc: yearSrc.payments) {
                Payment__c payment = new Payment__c(Year__c=year.Id,
                                        Lease__c=year.Lease__c,
                                        Warehouse__c=wh.value,
                                        Related_To_Company__c=year.Company__c,
                                        Payment_Type__c=paymentSrc.type,
                                        Payment_Method__c=paymentSrc.method,
                                        Payment_Amount__c=paymentSrc.amountWithoutVAT,
                                        CurrencyIsoCode='AED');

                List<Integer> allowedNumberOfPayments = new Integer[]{1, 2, 3, 4, 6, 12};

                if (allowedNumberOfPayments.contains(numberOfPayments)) {
                    payment.Cheque_Date__c = Peskov_Info_Lease.getChequeDate(paymentOrder, yearStartDate, numberOfPayments, gracePeriod);
                } 
                
                /** Payments can be grouped in a list and inserted after the loop */
                paymentsObjList.add(payment);

                paymentOrder++;
            }
        }

        insert paymentsObjList;

        return lease;
    }

    private static Date getChequeDate(Integer paymentOrder, Date yearStartDate, Integer numberOfPayments, Integer gracePeriod) {
        Date chequeDate = yearStartDate.addMonths(paymentOrder * 12 / numberOfPayments);
        
        //and now apply the grace period to ALL payments
        chequeDate = chequeDate.addDays(gracePeriod);

        return chequeDate;
    }

    @AuraEnabled(cacheable=true)
    static public List<String> getLeaseTypes() {
        String[] values = new List<String>();
        
        Schema.DescribeFieldResult fieldResult = Opportunity.Lease_Type_Status__c.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry pickListVal : ple){
            values.add(pickListVal.getLabel());
        }

        return values;
    }

}