@isTest
public class Peskov_Info_Lease_SearchTest {
    @isTest
    public static void testSearch() {
        Opportunity[] opps = new List<Opportunity>();
        Integer numberOfOpps = 10;
        for(Integer i=0; i < numberOfOpps; i++) {
            opps.add(new Opportunity(Name='Opportunity ' + i,
                                    CloseDate=Date.valueOf('2050-10-10'),
                                    StageName='Draft'));
        }
        insert opps;

        Id[] fixedSearchResults = new List<Id>();
        for(Integer i=0; i < 5; i++) {
            fixedSearchResults.add(opps[i].Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);

        String[] ids = new List<String>();
        LookupSearchResult[] results = Peskov_Info_Lease_Search.search('Oppo', ids);
        System.assertEquals(5, results.size());

        String idResult = results[0].getId();
        System.assertEquals(opps[0].Id, idResult);

        String icon = results[0].getIcon();
        System.assertEquals('standard:opportunity', icon);

        String objectType = results[0].getSObjectType();
        System.assertEquals('Opportunity', objectType);

        String title = results[0].getTitle();
        System.assertEquals('Opportunity 0', title);

        String subTitle = results[0].getSubtitle();
        System.assertEquals('Opportunity • Draft', subTitle);
    }
}
