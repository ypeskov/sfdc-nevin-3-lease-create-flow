@isTest
public class Peskov_Info_WarehouseTest {
    @isTest
    static public void testGetAllWarehouses() {
        Product2[] products = new List<Product2>();
        Integer numberOfWarehouses = 100;
        for(Integer i=0; i < numberOfWarehouses; i++) {
            products.add(new Product2(Name='Warehouse ' + i, IsActive=true));
        }

        insert products;

        Product2[] productsFound = Peskov_Info_Lease_Warehouse.getAllWarehouses();
        System.assertEquals(numberOfWarehouses, productsFound.size());
    }

    @isTest
    public static void testGetWarehousePrice() {
        Product2 product = new Product2(Name='TestWarehouse', Warehouse_Size__c=100);
        insert product;

        Decimal price = 1000.55;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = pricebookId, 
                                                        Product2Id = product.Id, 
                                                        CurrencyIsoCode='AED',
                                                        UnitPrice = price, 
                                                        IsActive = true);
        insert standardPBE;

        Decimal priceReturned = Peskov_Info_Lease_Warehouse.getWarehousePrice(standardPBE.Product2Id);
        System.assertEquals(price, priceReturned);
    }
}
