public with sharing class Peskov_Info_Lease_Warehouse {
    @AuraEnabled(cacheable=true)
    public static Product2[] getAllWarehouses() {
        String query = 'SELECT Id, Name, Warehouse_Size__c FROM Product2 JOIN  WHERE isActive = True';

        Product2[] warehouses = Database.query(query);

        return warehouses;
    }

    @AuraEnabled(cacheable=true)
    public static Decimal getWarehousePrice(String Id) {
        String query = 'SELECT UnitPrice from PricebookEntry WHERE Product2Id = \'' + String.escapeSingleQuotes(Id) + '\' ';

        PricebookEntry[] entry = Database.query(query);

        return entry[0].UnitPrice;
    }
}
