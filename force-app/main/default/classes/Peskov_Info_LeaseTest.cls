@isTest
public with sharing class Peskov_Info_LeaseTest {
    @isTest public static void testCreateLease() {
        
        String years = '[{"year":"2019","order":0,"startYearDate":"2019-11-30","endYearDate":"2020-11-29","rate":45,"size":7014,"totalPrice":315630,"numberOfPayments":2,"payments":[{"id":0,"type":"Open Fit Out Deposit","method":"Cheque","amountWithoutVAT":157815},{"id":1,"type":"Rental","method":"Cash","amountWithoutVAT":157815}]},{"year":"2020","order":1,"startYearDate":"2020-11-30","endYearDate":"2021-11-29","rate":45,"size":7014,"totalPrice":315630,"numberOfPayments":1,"payments":[{"id":0,"type":"Rental","method":"Cheque","amountWithoutVAT":315630}]}]';
        String leaseName = 'TestLease';
        String startDateStr = '2019-11-29';
        String endDateStr = '2021-11-29';
        String leaseType = 'New Lease';
        String closeDate = '2021-11-11';
        Integer gracePeriod = 30;

        Account acc = new Account(Name='Yura');
        insert acc;

        Opportunity prevLease = new Opportunity(Name='qqq', StageName='Draft', CloseDate=Date.valueOf('2025-12-12'));
        insert prevLease;

        Product2 product = new Product2(Name='TestWarehouse', Warehouse_Size__c=100);
        insert product;

        String warehouse = '{"label":"A1","value":"' + product.Id + '","size":7014,"rate":45,"totalPrice":315630}';

        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = pricebookId, 
                                                        Product2Id = product.Id, 
                                                        CurrencyIsoCode='AED',
                                                        UnitPrice = 1000, 
                                                        IsActive = true);
        insert standardPBE;

        Opportunity lease = Peskov_Info_Lease.createLease(
                                    leaseName, 
                                    leaseType, 
                                    String.valueOf(prevLease.Id), 
                                    String.valueOf(acc.Id),
                                    closeDate,
                                    startDateStr, 
                                    endDateStr, 
                                    warehouse, 
                                    years,
                                    gracePeriod);
        
        Opportunity resultedLease = [SELECT Lease_End_Date_2__c FROM Opportunity WHERE Id = :lease.Id];
        System.assertEquals(Date.valueOf('2021-12-28'), resultedLease.Lease_End_Date_2__c);
    }

    @isTest(SeeAllData=true)
    public static void testGetLeaseTypes() {
        String[] leaseTypes = Peskov_Info_Lease.getLeaseTypes();
        
        System.assertEquals(3, leaseTypes.size());
        System.assertEquals('New Lease', leaseTypes[0]);
        System.assertEquals('Renewed Lease', leaseTypes[1]);
    }
}
