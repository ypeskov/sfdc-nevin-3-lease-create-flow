@isTest
public class UpdateLeaseActiveStatusTest {
    @isTest
    public static void testActiveStatusIsDisabledForPreviousLease() {
        Date closeDate = Date.valueOf('2050-11-11');

        Opportunity oppPrevious = new Opportunity(Name='Previous Lease', Active__c=true, StageName='Draft', CloseDate=closeDate);
        insert oppPrevious;

        Opportunity oppNext = new Opportunity(Name='New Lease', 
                                            Active__c=false, 
                                            StageName='Draft', 
                                            CloseDate=closeDate, 
                                            Previous_Lease__c=oppPrevious.Id);
        insert oppNext;

        Test.startTest();
        
        oppNext.Active__c = true;
        update oppNext;

        Test.stopTest();

        Opportunity oppPrevAfterUpdate = [SELECT Active__c FROM Opportunity WHERE Id = :oppPrevious.Id];

        System.assertEquals(false, oppPrevAfterUpdate.Active__c);
    }
}
