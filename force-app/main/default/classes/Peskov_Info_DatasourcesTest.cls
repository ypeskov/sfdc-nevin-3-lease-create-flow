@isTest
public class Peskov_Info_DatasourcesTest {
    @isTest public static void testGetPaymentTypeValues() {
        List<String> paymentTypes = Peskov_Info_Datasources.getPaymentTypeValues();
        
        System.assertEquals(true, paymentTypes.size() > 0);
    }

    @isTest public static void testGetPaymentMethodValues() {
        String[] paymentMethods =  Peskov_Info_Datasources.getPaymentMethodValues();

        System.assertEquals(true, paymentMethods.size() > 0);
    }

    @isTest
    public static void testGetAccountNameById() {
        String accName = 'Test Account 1';
        Account acc = new account(Name=accName);
        insert acc;

        Account accFound = Peskov_Info_Datasources.getAccountNameById(acc.Id, 'Account');
        System.assertEquals(accName, accFound.Name);

        Opportunity opp = new Opportunity(Name='Opp1', 
                                        CloseDate=Date.valueOf('2025-10-10'), 
                                        AccountId=acc.Id,
                                        StageName='Draft');
        insert opp;

        accFound = new Account();
        accFound = Peskov_Info_Datasources.getAccountNameById(opp.Id, 'Opportunity');
        System.assertEquals(accName, accFound.Name);
    }
}
