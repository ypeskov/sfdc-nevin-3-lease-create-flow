// trigger UpdateOnActualOccupied on Actual_Occupied__c(after update, after insert) {
trigger UpdateOnActualOccupied on Actual_Occupied__c(after insert) {
    // trigger on Actual_Occupied__c object, not on Target
    // works only AFTER update & insert, so that we can have record's Id
    List<Actual_Occupied__c> aoToSetFalse = new List<Actual_Occupied__c>();   // my custom object
    List<Actual_Occupied__c> aoToSetTrue = new List<Actual_Occupied__c>();   // my custom object
    
    // Sets holding target ids (unique) and ActualOccupied ids (unique) respectively
	Set<Id> targetIds = new Set<Id>();
	Set<Id> actualoccupiedIds = new Set<Id>();
    
    for(Actual_Occupied__c ac : trigger.new){
            // replace "lookupfieldApiName" with actual lookup field Api name
            targetIds.add(ac.Monthly_Targets__c);
			actualoccupiedIds.add(ac.id);
    }

    // get those records from ActualOccupied object that have their parent Target Id's record...
    // stored in TargetIds and their id not present in ActualOccupiedIds
	aoToSetFalse = [SELECT id, Most_Recent__c FROM Actual_Occupied__c 
	      WHERE Most_Recent__c =true AND Monthly_Targets__c IN:targetIds AND Id NOT IN:actualoccupiedIds];

	for(Actual_Occupied__c ac : aoToSetFalse) {
		ac.Most_Recent__c = false;
    }
    update aoToSetFalse;

    //and finally let's set rue to all Actual_Occupied that are a record of this trigger, assuming they are the last
    aoToSetTrue  = [SELECT Id, Most_Recent__c FROM Actual_Occupied__c WHERE Id IN:actualoccupiedIds];
    for(Actual_Occupied__c ac : aoToSetTrue) {
        ac.Most_Recent__c = true;
    }
    update aoToSetTrue;
}