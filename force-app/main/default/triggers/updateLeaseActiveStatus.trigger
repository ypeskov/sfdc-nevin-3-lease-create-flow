trigger updateLeaseActiveStatus on Opportunity (after update) {
    if (LeaseTriggerUpdateHandler.isFirstTime == true) {
        //disable double firing of the trigger
        LeaseTriggerUpdateHandler.isFirstTime = false;

        Integer qtyOpportunites = Trigger.new.size();
        for(Integer i=0; i < qtyOpportunites; i++) {
            Opportunity newOpp = Trigger.new[i];
            Opportunity oldOpp = Trigger.old[i];

            if (newOpp.Active__c == true && newOpp.Active__c != oldOpp.Active__c) {
                if (newOpp.Previous_Lease__c != null) {
                    Opportunity prevOpp = [SELECT Name, Active__c FROM Opportunity WHERE Id = :newOpp.Previous_Lease__c];
                    prevOpp.Active__c = false;
                    update prevOpp;
                }
            }
        }
        
    }
}