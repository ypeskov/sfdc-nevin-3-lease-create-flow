trigger UpdateTargetOnActualOccupied on Actual_Occupied__c (before insert, before update) {
    for(Actual_Occupied__c ao : Trigger.new) {
        Targets__c[] items = [SELECT Id, Month__c  FROM Targets__c WHERE Month__c = :ao.Month__c];
        if (items.size() > 0) {
            Targets__c item = items[0];
            ao.Monthly_Targets__c  = item.Id;

            update item;
        } else {
            // TODO: if no Targets__c is found
            // This is test line of code for verification
        }
        
    } 
}